Project Work:
Design a Post and Comment system just like you do on any social media(FB, LinkedIn), where
users are able to create post, like it and comment on it

App functionality:

1.	Users are able to create posts
2.	Users are able to view 5 latest posts max at any given time of any user
3.	Users can like the post
4.	Users can comment on any post
5.	User can view 3 latest comments for any given post
6.	User who created the post is only allowed to update/delete their post
7.	Users can get the number of likes for the post

Tasks:
•	Clone and Pull the codebase from this repo
•	Create a branch <YOUR-NAME>-assessment-app
•	Design the ERD in mysql workbench
•	Create a FastAPI project(with APIs) which does the above mentioned
•	Copy the ERD,any Native Query created in a sql file into you codebase and Push the code to your branch
